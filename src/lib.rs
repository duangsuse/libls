#![no_std]
#![feature(lang_items)]

extern crate libc;
use core::any::Any;
use libc::{access, F_OK, c_char, fopen, fgetc, EOF, fclose, fprintf};


const PATH_PRE: &'static str = "/sdcard/";
const PATH_DATA: &'static str = "Android/org.duangsuse.ls/";
const SWAPFILE: &'static str = "swap";

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Java_org_duangsuse_ls_score_scan(_: *const Any, _: *const Any) {}

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Java_org_duangsuse_ls_score_clean(_: *const Any, _: *const Any) {}

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Java_org_duangsuse_ls_score_hello(_: *const Any, _: *const Any) -> i32 {
    666
}

#[lang = "eh_personality"]
extern "C" fn eh_personality() {}
#[lang = "panic_fmt"]
fn panic_fmt() -> ! {
    loop {}
}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {}
}
